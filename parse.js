// Graceful fs have some better stuff on mac?
// const fs = require('graceful-fs')
const fs = require('fs')
const { promisify } = require('util')
const readDir = promisify(fs.readdir)
const readFile = promisify(fs.readFile)
const path = require('path')

const output = fs.createWriteStream(path.join(__dirname,'output.sql'))
const dir = 'download'

// we can use await for promices only inside async function
async function parse() {
  const files = await readDir(path.join(__dirname, dir))
  for (const file of files){
    const fileRaw = await readFile(path.join(__dirname, dir,file))
    const fileJson = JSON.parse(fileRaw)
    for (const item of fileJson.items){
      // do the SQL insert here
      output.write(`INSERT Blalba ${item.id} 'name'=${item.name};\n`)
    }
  }
}

parse()
