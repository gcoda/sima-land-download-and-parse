const got = require('got')
const fs = require('fs')
const simUrl = (id, perPage = 50) =>
  `https://www.sima-land.ru/api/v3/item/?per-page=${perPage}&id-greater-than=${id}&expand=description,attrs,categories`

let count = 0

const dl = (id = 0) => {
  got(simUrl(id)).then(res => {
    fs.writeFile(
      `${__dirname}/download/${leadZero(id,10)}.json`,
      res.body,
      'utf8',
      err => {
        if (err) throw err
        let data = JSON.parse(res.body)
        if (data.items && data.items.length){
          let lastItem = data.items[data.items.length - 1]
          count += data.items.length
          console.log('total items: ', count)
          dl(lastItem.id)
        }
      }
    )
  })
  .catch(err => {
    console.log('404 means - we are done?')
  })
}

const leadZero = (number, width) => {
  width -= number.toString().length
  if (width > 0) {
    return (
      new Array(width + (/\./.test(number) ? 2 : 1)).join(
        '0'
      ) + number
    )
  }
  return number + ''
}

dl()
